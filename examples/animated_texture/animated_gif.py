from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.image import Image
from kivy.app import App
from kivy.graphics import Rectangle


class MainApp(App):
    def __init__(self, **kwargs):
        super(MainApp, self).__init__(**kwargs)
        self.root = RelativeLayout()

        animated_icon = Image(source='test.gif', anim_delay=0.001)
        animated_icon.bind(texture=self.update_texture)

        self.r = Rectangle(texture=animated_icon.texture, size=(500, 255), pos=(100, 100))
        self.root.canvas.add(self.r)

    def update_texture(self, instance, value):
        self.r.texture = value

    def build(self):
        return self.root


if __name__ == '__main__':
    MainApp().run()