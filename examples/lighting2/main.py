import os
import math
from kivy.app import App
from kivy.uix.effectwidget import EffectWidget, FXAAEffect
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import ListProperty
from kivy.clock import Clock
from kivy.core.image import Image

from kivy3 import Scene, Renderer, PerspectiveCamera
from kivy3.extras.geometries import PlaneGeometry, ConeGeometry, BoxGeometry, CylinderGeometry, SphereGeometry, GridGeometry, TubeGeometry
from kivy3.extras.objects import ArrowObject, AxisObject
from kivy3.loaders import URDFLoader, STLLoader
from kivy3 import Mesh, Material, Object3D
from kivy3.objects.lines import Lines
from kivy3.widgets import OrbitControlWidget, SelectionWidget, Object3DWidget

from kivy.graphics.opengl import glEnable, glDisable, GL_DEPTH_TEST, glReadPixels, GL_RGBA, GL_UNSIGNED_BYTE




_this_path = os.path.dirname(os.path.realpath(__file__))
shader_file = os.path.join(_this_path, "./blinnphong_test.glsl")
obj_file = os.path.join(_this_path, "./monkey.obj")
stl_file = os.path.join(_this_path, "./inner_jaw_link.STL")
urdf_file = os.path.join(_this_path, "./rs1_description/urdf/generated_rs1_parallel.urdf")
package_path = os.path.join(_this_path, "./") # parent of the package path
arrow_img_file = os.path.join(_this_path, "./assets/icon-rotate-360.png")
prismatic_arrow_img_file = os.path.join(_this_path, "./assets/icon-arrows.png")




class VisualisationWidget(FloatLayout):
    def __init__(self, **kw):
        super(VisualisationWidget, self).__init__(**kw)

        self.renderer = Renderer(shader_file=shader_file)
        self.renderer.set_clear_color((0.5,0.3,0.8,1))

        self.renderer.main_light.pos = 1, 1, 0
        self.renderer.main_light.intensity = 0.0

        geometry = SphereGeometry(0.05)
        material = Material(color=(1, 1, 1), ambient=(1, 1, 1), diffuse=(1, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        light = Mesh(geometry, material)
        light.pos.x = 1
        light.pos.y = 1
        light.pos.z = 0

        self.scene = Scene()
        self.base = Object3D()
        self.base.add(light)
        # id_color = (1,0,0)
        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3,0,0), ambient=(1, 0, 0), diffuse=(1, 0, 0), specular=(0.3,0.3,0.3), transparency=1)
        object1 = Mesh(geometry, material)
        object1.pos.x = 1
        object1.pos.y = 0
        object1.pos.z = 0
        self.base.add(object1)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(0, 1, 0), diffuse=(1, 0, 0), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object2 = Mesh(geometry, material)
        object2.pos.x = -1
        object2.pos.y = 0
        object2.pos.z = 0
        self.base.add(object2)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(0, 0, 1), diffuse=(0, 0, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object3 = Mesh(geometry, material)
        object3.pos.x = 0
        object3.pos.y = 1
        object3.pos.z = 0
        self.base.add(object3)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(1, 1, 0), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object4 = Mesh(geometry, material)
        object4.pos.x = 0
        object4.pos.y = -1
        object4.pos.z = 0
        self.base.add(object4)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(1, 0, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object5 = Mesh(geometry, material)
        object5.pos.x = 0
        object5.pos.y = 0
        object5.pos.z = 1
        self.base.add(object5)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = 0
        object6.pos.y = 0
        object6.pos.z = -1
        self.base.add(object6)

        geometry = BoxGeometry(0.3,0.3,0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = 1
        object6.pos.y = 1
        object6.pos.z = 1
        object6.rot.x = 180
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = 1
        object6.pos.y = 1
        object6.pos.z = -1
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = 1
        object6.pos.y = -1
        object6.pos.z = 1
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = -1
        object6.pos.y = 1
        object6.pos.z = 1
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = 1
        object6.pos.y = -1
        object6.pos.z = -1
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = -1
        object6.pos.y = -1
        object6.pos.z = 1
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = -1
        object6.pos.y = 1
        object6.pos.z = -1
        self.base.add(object6)

        geometry = SphereGeometry(0.3)
        material = Material(color=(0.3, 0, 0), ambient=(1, 0.0, 1), diffuse=(0, 1, 1), specular=(0.3, 0.3, 0.3),
                            transparency=1)
        object6 = Mesh(geometry, material)
        object6.pos.x = -1
        object6.pos.y = -1
        object6.pos.z = -1
        self.base.add(object6)

        material = Material(color=(1, 0, 1), diffuse=(1, 0, 1),
                            specular=(0.35, .35, .35), transparency=1)
        stl_loader = STLLoader()
        stl_object = stl_loader.load(stl_file, material)
        stl_object.pos.z = 3
        stl_object.scale = (10, 10, 10)

        self.base.add(stl_object)

        material = Material(color=(1, 0, 1), diffuse=(1, 0, 1),
                            specular=(0.35, .35, .35), transparency=1)
        stl_loader = STLLoader()
        stl_object = stl_loader.load(stl_file, material)
        stl_object.pos.z = 3
        stl_object.pos.x = 3
        stl_object.rot.x = 180
        stl_object.scale = (10, 10, 10)

        self.base.add(stl_object)


        # geometry = CylinderGeometry(0.25, 0.5)
        # material = Material(color=(0,0.3,0), diffuse=(0,0.3,0), specular=(0.3,0.3,0.3))
        # object = Mesh(geometry, material)
        # object.pos.y = 1
        # self.base.add(object)
        # geometry = ConeGeometry(0.25, 0.5)
        # material = Material(color=(0,0.3,0.3), diffuse=(0,0.3,0.3), specular=(0.3,0.3,0.3))
        # object = Mesh(geometry, material)
        # object.pos.y = -1
        # self.base.add(object)
        # geometry = SphereGeometry(radius=0.25)
        # material = Material(color=(0,0,1), diffuse=(0,0,1), specular=(0.3,0.3,0.3))
        # object = Mesh(geometry, material)
        # object.pos.x = -1
        # self.base.add(object)
        #
        # geometry = GridGeometry()
        # material = Material(color=(1., 1., 1.), diffuse=(1., 1., 1.),
        #                     specular=(.35, .35, .35), transparency=.3)
        # object = Lines(geometry, material)
        # self.base.add(object)
        #
        # material = Material(color=(0.3, 0, 0.3), diffuse=(0.3, 0, 0.3),
        #                     specular=(.35, .35, .35), transparency=0.5)
        # stl_loader = STLLoader()
        # stl_object = stl_loader.load(stl_file, material)
        # stl_object.pos.z = 1
        # stl_object.scale = (10,10,10)

        #self.base.add(stl_object)

        self.base.rot.x = -90
        self.scene.add(self.base)

        self.camera = PerspectiveCamera(90, 0.3, 0.1, 1000)
        self.camera.pos.z = 1.5
        self.camera.pos.x = 0.0
        self.camera.pos.y = 0.0
        self.camera.look_at((0, 0, 0))

        self.camera.bind_to(self.renderer)
        self.renderer.render(self.scene, self.camera)
        w = EffectWidget()
        w.add_widget(self.renderer)
        # self.add_widget(self.renderer, index=30)
        self.orbit = OrbitControlWidget(self.renderer, 4.)
        w.add_widget(self.orbit)
        #w.effects = [FXAAEffect()]
        self.add_widget(w)
        # self.add_widget(self.selection_widget, index=98)
        self.renderer.bind(size=self._adjust_aspect)
        # Clock.schedule_once(self.delayed_add, 3)



    def _adjust_aspect(self, inst, val):
        rsize = self.renderer.size
        aspect = rsize[0] / float(rsize[1])
        self.renderer.camera.aspect = aspect


    def delayed_add(self, *dt):
        geometry = TubeGeometry(outer_radius=0.5, inner_radius=0.25, length=0.5)
        material = Material(color=(1., 0., 1.), diffuse=(1., 0., 1.),
                            specular=(.35, .35, .35))
        object = Mesh(geometry, material)
        object.pos.x = 1
        self.base.add_object(object)

class VisualisationApp(App):
    def build(self):

        return VisualisationWidget()


if __name__ == '__main__':
    from kivy.config import Config
    # Config.set('input', 'mouse', 'mouse,disable_multitouch')
    VisualisationApp().run()
