import os
import math
from kivy.app import App
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.scatterlayout import ScatterLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.properties import ListProperty
from kivy.clock import Clock
from kivy.core.image import Image

from kivy3 import Scene, Renderer, PerspectiveCamera
from kivy3.extras.geometries import PlaneGeometry, ConeGeometry, BoxGeometry, CylinderGeometry, SphereGeometry, \
    GridGeometry, TubeGeometry, AnnularSector
from kivy3.extras.objects import ArrowObject, AxisObject, CircularProgressBar
from kivy3.loaders import URDFLoader
from kivy3 import Mesh, Material, Object3D
from kivy3.objects.lines import Lines
from kivy3.widgets import OrbitControlWidget, SelectionWidget, Object3DWidget

from kivy.graphics.opengl import glEnable, glDisable, GL_DEPTH_TEST, glReadPixels, GL_RGBA, GL_UNSIGNED_BYTE

_this_path = os.path.dirname(os.path.realpath(__file__))
shader_file = os.path.join(_this_path, "../blinnphongv3.glsl")
obj_file = os.path.join(_this_path, "./monkey.obj")
stl_file = os.path.join(_this_path, "./test.stl")
urdf_file = os.path.join(_this_path, "./rs1_description/urdf/generated_rs1_parallel.urdf")
package_path = os.path.join(_this_path, "./")  # parent of the package path
arrow_img_file = os.path.join(_this_path, "./assets/icon-rotate-360.png")
prismatic_arrow_img_file = os.path.join(_this_path, "./assets/icon-arrows.png")


class VisualisationWidget(FloatLayout):
    def __init__(self, **kw):
        super(VisualisationWidget, self).__init__(**kw)

        self.renderer = Renderer(shader_file=shader_file, supersample=2)
        self.renderer.set_clear_color((.16, .30, .44, 1.))

        self.scene = Scene()

        self.base = Object3D()
        # id_color = (1,0,0)

        self.cpb = CircularProgressBar(6, 180., 0.05, 0.04, 0.005)
        self.cpb.pos.y = 0
        self.base.add(self.cpb)
        self.base.rot.x = -90
        self.scene.add(self.base)

        self.camera = PerspectiveCamera(90, 0.005, 0.1, 1000)
        self.camera.pos.z = 1.5
        self.camera.look_at((0, 0, 0))

        self.camera.bind_to(self.renderer)
        self.renderer.render(self.scene, self.camera)

        self.add_widget(self.renderer, index=30)
        self.orbit = OrbitControlWidget(self.renderer, 4.)
        self.add_widget(self.orbit, index=99)
        # self.add_widget(self.selection_widget, index=98)
        self.renderer.bind(size=self._adjust_aspect)

        self.current_progress = 1.
        Clock.schedule_interval(self.update_progress, 1. / 30.)

    def _adjust_aspect(self, inst, val):
        rsize = self.renderer.size
        aspect = rsize[0] / float(rsize[1])
        self.renderer.camera.aspect = aspect

    def update_progress(self, dt):
        self.cpb.set_progress(self.current_progress)
        # self.current_progress = 1
        if self.current_progress >= 100:
            self.current_progress = -100
        else:
            self.current_progress += 0.3

class VisualisationApp(App):
    def build(self):
        return VisualisationWidget()


if __name__ == '__main__':
    from kivy.config import Config

    # Config.set('input', 'mouse', 'mouse,disable_multitouch')
    VisualisationApp().run()
