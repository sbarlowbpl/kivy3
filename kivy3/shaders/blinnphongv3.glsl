---VERTEX SHADER-------------------------------------------------------
/**
* Based on: https://learnopengl.com/Advanced-Lighting/Advanced-Lighting
*/
# version 130
#ifdef GL_ES
    precision highp float;
#endif

attribute vec3  v_pos;
attribute vec3  v_normal;
attribute vec4  v_color;
attribute vec2  v_tc0;

uniform mat4 modelview_mat;
uniform mat4 projection_mat;
uniform mat4 model_mat;
uniform mat4 view_mat;


varying vec4 frag_color;
varying vec2 uv_vec;
varying vec3 normal_vec;
varying vec3 vertex_pos;



void main (void) {
    vec4 pos = modelview_mat * vec4(v_pos,1.0);
    //vertex_pos = pos;
    vertex_pos = vec3(model_mat * pos);//vec4(pos,1.0);
    gl_Position = projection_mat * pos;
    // vertex_pos = model_mat * vec4(v_pos,1.0);
    frag_color = v_color;
    uv_vec = v_tc0;
    //normal_vec = modelview_mat * vec4(v_normal, 0.0);
    normal_vec = vec3(model_mat * modelview_mat * vec4(v_normal, 0.0));

}


---FRAGMENT SHADER-----------------------------------------------------
#ifdef GL_ES
    precision highp float;
#endif

varying vec4 frag_color;
varying vec2 uv_vec;
varying vec3 normal_vec;
varying vec3 vertex_pos;

uniform mat4 normal_mat;
uniform sampler2D tex;
uniform float tex_ratio;

uniform vec3 light_pos;
uniform vec3 light_color;
uniform float light_intensity;
uniform vec3 camera_pos;

uniform vec3 Ka; // color (ambient)
uniform vec3 Kd; // diffuse color
uniform vec3 Ks; // specular color
uniform float Tr; // transparency
uniform float Ns; // shininess
uniform float d; // dissolve

void main (void){
    if (Tr < 0.1){
        discard;
    }
    else{
        vec3 light_pos0 = vec3(5,5,10);
        vec3 light_pos1 = vec3(-5,-5,10);

        vec4 tex_color = texture2D(tex, uv_vec);

        vec3 ambient_color = vec3(tex_ratio*tex_color) + Ka*(1.0-tex_ratio);
        // ambient
        vec3 ambient = light_color * ambient_color * 0.3;

        //diffuse
        vec3 lightDir = normalize(light_pos0 - vertex_pos);
        vec3 normal = normalize(vec3(normal_vec));
        float diff = max(dot(lightDir, normal), 0.0);

        vec3 diffuse_color = vec3(tex_ratio*tex_color) + Kd*(1.0-tex_ratio);
        vec3 diffuse = diff * diffuse_color * light_color;

        // specular
        vec3 viewDir = normalize(camera_pos - vertex_pos);
        vec3 reflectDir = reflect(-lightDir, normal);
        float spec = pow(max(dot(viewDir, reflectDir), 0.0), Ns);
        vec3 specular_color = vec3(tex_ratio*tex_color) + Ks*(1.0-tex_ratio);
        vec3 specular = light_color * spec * specular_color;
        float Tr_color = tex_ratio*tex_color[3] + Tr*(1.0-tex_ratio);

        //diffuse
        vec3 lightDir2 = normalize(light_pos1 - vertex_pos);
        vec3 normal2 = normalize(vec3(normal_vec));
        float diff2 = max(dot(lightDir2, normal2), 0.0);

        vec3 diffuse_color2 = vec3(tex_ratio*tex_color) + Kd*(1.0-tex_ratio);
        vec3 diffuse2 = diff2 * diffuse_color * light_color;

        // specular
        vec3 viewDir2 = normalize(camera_pos - vertex_pos);
        vec3 reflectDir2 = reflect(-lightDir2, normal);
        float spec2 = pow(max(dot(viewDir2, reflectDir2), 0.0), Ns);
        vec3 specular_color2 = vec3(tex_ratio*tex_color) + Ks*(1.0-tex_ratio);
        vec3 specular2 = light_color * spec2 * specular_color2;


        gl_FragColor = vec4(vec3(ambient + diffuse +diffuse2 + specular + specular2), Tr_color);
    }
}
