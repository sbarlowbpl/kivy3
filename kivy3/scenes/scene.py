
"""
The MIT License (MIT)

Copyright (c) 2013 Niko Skrypnik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from copy import deepcopy

from kivy.graphics import Callback, UpdateNormalMatrix
from kivy.graphics.opengl import glEnable, glDisable, GL_DEPTH_TEST, GL_TRUE, glDepthMask, GL_FALSE, glColorMask, \
    glDepthFunc, GL_LEQUAL, GL_LESS
from kivy3.core.object3d import Object3D


class Scene(Object3D):
    """ Scene object """
    instruction_map = {}

    def __init__(self):
        super(Scene, self).__init__()

    def as_instructions(self):
        if not self._instructions.children:
            for child in self.get_children_instructions():
                self._instructions.add(child)

        self._instructions.add(Callback(self.disable_color))

        if not self._transparent_instructions.children:
            for child in self.get_children_transparent_instructions():
                self._instructions.add(child)
        self._instructions.add(Callback(self.enable_color))

        self._instructions.add(Callback(self.enable_special_depth))
        if not self._transparent_instructions.children:
            for child in self.get_children_transparent_instructions():
                self._instructions.add(child)
        self._instructions.add(Callback(self.disable_special_depth))

        return self._instructions


    def disable_color(self, instr):
        glColorMask(0, 0, 0, 0)

    def enable_color(self, instr):
        glColorMask(1, 1, 1, 1)



    def enable_special_depth(self, inst):
        glDepthFunc(GL_LEQUAL)

    def disable_special_depth(self, inst):
        glDepthFunc(GL_LESS)

    def enable_depth(self, instr):

        glDepthMask(GL_TRUE)
        # self.renderer.fbo.clear_buffer()

    def disable_depth(self, instr):
        glDepthMask(GL_FALSE)
