import os
import platform
from kivy.graphics.context_instructions import PushMatrix, PopMatrix, Color
from kivy.graphics.fbo import Fbo
from kivy.graphics.instructions import Canvas, Callback, InstructionGroup
from kivy.graphics.opengl import glEnable, GL_DEPTH_TEST, glDisable
from kivy.graphics.vertex_instructions import Rectangle
from kivy.input.providers.mouse import MouseMotionEvent
if platform.system() == 'Windows':
    from kivy.input.providers.wm_touch import WM_MotionEvent
else:
    class WM_MotionEvent:
        pass
from kivy.uix.floatlayout import FloatLayout
from kivy.graphics.transformation import Matrix

from kivy.uix.relativelayout import RelativeLayout
from kivy.core.image import Image
from kivy.core.window import Window
from kivy.graphics.texture import Texture
from kivy.clock import Clock
from functools import partial

import copy
import struct

from kivy.uix.widget import Widget

import kivy3
from kivy3 import Renderer
from kivy3.light import Light
from kivy3.renderer import RendererError

_this_path = os.path.dirname(os.path.realpath(__file__))
select_mode_shader = os.path.join(_this_path, 'select_mode.glsl')
kivy3_path = os.path.abspath(os.path.dirname(kivy3.__file__))


class SelectionRenderer(Widget):

    def __init__(self, **kw):
        self.shader_file = kw.pop("shader_file", None)
        self.canvas = Canvas()
        super(SelectionRenderer, self).__init__()

        with self.canvas:
            clear_color = Color(rgba=(0, 0, 0, 1))
            self._viewport = Rectangle(size=self.size, pos=self.pos)
            self.fbo = Fbo(size=self.size,
                           with_depthbuffer=True, compute_normal_mat=True,
                           clear_color=(0, 0, 0, 1))
        self._config_fbo()
        self.texture = self.fbo.texture
        self.camera = None
        self.scene = None
        self.main_light = Light(renderer=self, pos=(0, 1, 0))

    def _config_fbo(self):
        # set shader file here
        self.fbo.shader.source = self.shader_file or \
            select_mode_shader

        #self.fbo.shader.vs = shader_vs
        #self.fbo.shader.fs = shader_fs
        with self.fbo:
            Callback(self._setup_gl_context)
            PushMatrix()
            # instructions set for all instructions
            self._instructions = InstructionGroup()
            PopMatrix()
            Callback(self._reset_gl_context)

    def _setup_gl_context(self, *args):
        glEnable(GL_DEPTH_TEST)
        self.fbo.clear_buffer()

    def _reset_gl_context(self, *args):
        glDisable(GL_DEPTH_TEST)

    def render(self, scene, camera):
        self.scene = scene
        self.camera = camera
        #self.camera.bind_to(self)
        self._instructions.add(scene.as_instructions())
        self._update_matrices()
        Clock.schedule_once(self._update_matrices, -1)

    def on_size(self, instance, value):
        self.fbo.size = value
        self._viewport.texture = self.fbo.texture
        self._viewport.size = value
        self._viewport.pos = self.pos
        self._update_matrices()

    def on_pos(self, instance, value):
        self._viewport.pos = self.pos
        self._update_matrices()

    def on_texture(self, instance, value):
        self._viewport.texture = value

    def _update_matrices(self, dt=None):
        if self.camera:
            self.fbo['projection_mat'] = self.camera.projection_matrix
            self.fbo['modelview_mat'] = self.camera.modelview_matrix
            self.fbo['model_mat'] = self.camera.model_matrix
            self.fbo['camera_pos'] = [float(p) for p in self.camera.position]
            self.fbo['view_mat'] = Matrix().rotate(
                Window.rotation, 0.0, 0.0, 1.0)
        else:
            raise RendererError("Camera is not defined for renderer")

    def set_clear_color(self, color):
        self.fbo.clear_color = color



class SelectionWidget(RelativeLayout):
    def __init__(self, renderer, **kw):
        super(SelectionWidget, self).__init__()
        self.object_dict = {}
        self.renderer = renderer
        self.touch = None
        self.last_touched_object = None
        #print(__name__, "fbo:size", self.renderer.fbo.texture.size)
        self.last_sel_image = Texture.create(self.renderer.fbo.texture.size, colorfmt='rgba', bufferfmt='ubyte')
        self.last_hover_color = (0,0,0)

        self.mouse_pos = []
        self.last_mouse_pos = []
        Window.bind(mouse_pos=self.on_move)
        self.mouse_over_clock = Clock.schedule_interval(self.move_cycle, 0)

        self.last_scroll = None
        self.grabbed = False

        self.selection_vs = None
        self.selection_fs = None




        self.selection_renderer = SelectionRenderer()

        self.selection_renderer.size = self.renderer.size
        self.selection_renderer.pos = self.renderer.pos

        self.renderer.bind(size=self.update_selection_renderer)
        self.renderer.bind(pos=self.update_selection_renderer)

        #camera.bind_to_selection_renderer(self.selection_renderer)


    def update_selection_renderer(self, widget, value):
        self.selection_renderer.size = self.renderer.size
        self.selection_renderer.pos = self.renderer.pos

        #print("Updating:", self.renderer.size, self.renderer.pos)
        pass

    def register(self, id, widget):
        id = tuple(id)
        self.object_dict[tuple(id)] = widget
        #self.add_widget(widget)

    def unregister(self, id):
        id = tuple(id)
        if id in self.object_dict.keys():
            #self.remove_widget(self.object_dict[id])
            self.object_dict.pop(id)

    def get_available_id(self, reserve = True):
        """Get an Id that is unused. there is an option to reserve that address"""
        for i in range(200,256):
            for j in range(256):
                for k in range(256):
                    color_id = tuple([i, j, k])
                    if color_id not in self.object_dict.keys() and color_id != (0, 0, 0):
                        if reserve:
                            self.object_dict[color_id] = None
                        return color_id

    def on_touch_down(self, touch):
        self.selection_renderer._update_matrices()
        self.grabbed = True
        if (isinstance(touch, MouseMotionEvent) and touch.button != "scrolldown" and touch.button != "scrollup" \
                or (platform.system() == 'Windows' and isinstance(touch, WM_MotionEvent)))\
                and self.collide_point(*touch.pos):
            widget = self.get_clicked_object(touch)
            if widget is not None:
                # print("Touched down")
                self.last_touched_object = widget
                return widget.on_object_touch_down(touch)

        # def on_touch_move(self, touch):
        #     if self.collide_point(*touch.pos):
        #         widget = self.get_clicked_object(touch)
        #         if widget is not None:
        #             return widget.on_object_touch_move(touch)

    def on_touch_up(self, touch):
        self.selection_renderer._update_matrices()
        self.grabbed = False
        if isinstance(touch, MouseMotionEvent) and (touch.button == "scrolldown" or touch.button == "scrollup"):
            self.last_scroll = touch
            #Clock.schedule_once(partial(self.update_blit, touch, True), 0.2)
            return

        if self.last_touched_object:
            widget = self.last_touched_object
            self.last_touched_object = None
            #self.update_blit( touch, False, 0)
            return widget.on_object_touch_up(touch)
        if (isinstance(touch, MouseMotionEvent) and touch.button != "scrolldown" and touch.button != "scrollup" \
                or (platform.system() == 'Windows' and isinstance(touch, WM_MotionEvent)))\
                and self.collide_point(*touch.pos):
            widget2 = self.get_clicked_object(touch)
            if widget2 is not None:
                # print("Touched up")
                #self.update_blit( touch, False, 0)
                return widget2.on_object_touch_up(touch)



    def update_blit(self, touch, scroll, dt):
        if scroll and self.last_scroll is not touch:
            return

        # original_fs = self.renderer.fbo.shader.fs
        # original_vs = self.renderer.fbo.shader.vs
        # # original_source = self.renderer.fbo.shader.source
        # original_clear_color = self.renderer.fbo.clear_color
        # if self.selection_fs is None:
        #     self.renderer.fbo.shader.source = select_mode_shader
        # else:
        #     self.renderer.fbo.shader.fs = self.selection_fs
        #     self.renderer.fbo.shader.vs = self.selection_vs
        # self.renderer.set_clear_color((0., 0., 0., 0.))
        # self.renderer.fbo.ask_update()
        # self.renderer.fbo.draw()
        self.selection_renderer.fbo.draw()
        # if self.selection_renderer.fbo.size != self.last_sel_image.size:
        #     self.last_sel_image = Texture.create(self.selection_renderer.fbo.texture.size, colorfmt='rgba', bufferfmt='ubyte')
        # self.last_sel_image.blit_buffer(self.selection_renderer.fbo.pixels, colorfmt="rgba", bufferfmt="ubyte",
        #                                 size=self.selection_renderer.fbo.size)
        # print(color)

        # if self.selection_fs is None:
        #     self.selection_fs = self.renderer.fbo.shader.fs
        #     self.selection_vs = self.renderer.fbo.shader.vs
        # self.renderer.fbo.shader.vs = original_vs
        # self.renderer.fbo.shader.fs = original_fs
        # self.renderer.set_clear_color(original_clear_color)
        # self.renderer.fbo.ask_update()
        self.renderer.fbo.draw()

    def on_move(self, type, pos):
        self.mouse_pos = pos

    # def on_move(self, type, pos):
    def move_cycle(self, *dt):
        if self.grabbed or (self.mouse_pos == self.last_mouse_pos):
            self.last_mouse_pos = self.mouse_pos
            return
        self.last_mouse_pos = self.mouse_pos

        pos = self.mouse_pos
        #if self.last_sel_image is not None:
            # print(__name__, "on_move()", pos)

            #pos = self.parent.to_parent(touch.x, touch.y)

        #self.selection_renderer.fbo.draw()

        color = tuple(
            self.selection_renderer.fbo.get_pixel_color(pos[0] - self.parent.pos[0], pos[1] - self.parent.pos[1])[
            0:3])

        #self.renderer.fbo.draw()

        #self.renderer.fbo.draw()
        #pixel=self.last_sel_image.get_region(*pos, 1,1)
        #bp = pixel.pixels
        #color = struct.unpack('4B', bp)
        # print(__name__, "on_move()", color)
        color = tuple(color[0:3])
        if color != self.last_hover_color:
            if self.last_hover_color in self.object_dict:
                if self.object_dict[self.last_hover_color] is not None:
                    widget = self.object_dict[self.last_hover_color]
                    widget.on_object_hover_off()


            self.last_hover_color = color
            if color in self.object_dict:
                if self.object_dict[color] is not None:
                    widget = self.object_dict[color]
                    widget.on_object_hover_on()
                    #self.renderer.fbo.ask_update()
        self.renderer.fbo.ask_update()
        self.renderer.fbo.draw()


    def get_clicked_object(self, touch):

        # original_fs = self.renderer.fbo.shader.fs
        # original_vs = self.renderer.fbo.shader.vs
        # #original_source = self.renderer.fbo.shader.source
        # original_clear_color = self.renderer.fbo.clear_color
        #
        # if self.selection_fs is None:
        #     self.renderer.fbo.shader.source = select_mode_shader
        # else:
        #     self.renderer.fbo.shader.fs = self.selection_fs
        #     self.renderer.fbo.shader.vs = self.selection_vs
        #
        # self.renderer.fbo.clear_color = (0., 0., 0., 0.)
        #self.selection_renderer._update_matrices()
        self.selection_renderer.fbo.ask_update()
        self.selection_renderer.fbo.draw()
        pos = self.parent.to_parent(touch.x,touch.y)
        color = tuple(self.selection_renderer.fbo.get_pixel_color(pos[0]-self.parent.pos[0],pos[1]-self.parent.pos[1])[0:3])
        #print(color)
        # self.last_sel_image = Image(copy.deepcopy(self.renderer.fbo.texture))
        # self.last_sel_image.size = self.renderer.fbo.size
        # if self.selection_renderer.fbo.size != self.last_sel_image.size:
        #     self.last_sel_image = Texture.create(self.selection_renderer.fbo.texture.size, colorfmt='rgba', bufferfmt='ubyte')
        # self.last_sel_image.blit_buffer(self.selection_renderer.fbo.pixels, colorfmt="rgba", bufferfmt="ubyte", size=self.selection_renderer.fbo.size)


        # if self.selection_fs is None:
        #     self.selection_fs = self.renderer.fbo.shader.fs
        #     self.selection_vs = self.renderer.fbo.shader.vs
        #
        #
        # self.renderer.fbo.shader.vs = original_vs
        # self.renderer.fbo.shader.fs = original_fs
        # #self.renderer.fbo.shader.source = original_source
        # self.renderer.set_clear_color(original_clear_color)
        self.renderer.fbo.ask_update()
        self.renderer.fbo.draw()

        if color in self.object_dict:
            if self.object_dict[color] is not None:
                return self.object_dict[color]
        return None

