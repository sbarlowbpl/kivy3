import colorsys
from copy import copy

from kivy.clock import Clock
from kivy.uix.video import Video
from kivy3.extras.geometries import ConeGeometry, CylinderGeometry, AnnularSector, TubeGeometry, PlaneGeometry
from kivy3 import Object3D, Mesh, Material
import math

class ArrowObject(Object3D):
    def __init__(self, material,**kw):

        length = kw.pop('length', 1.)
        radius = kw.pop('radius', length/10.)
        cone_radius = kw.pop('cone_radius', radius*2.)
        cone_length = kw.pop('cone_length', radius*3.)
        cylinder_length = kw.pop('cylinder_length', length-(radius*3.))
        super(ArrowObject, self).__init__(**kw)
        cone = ConeGeometry(cone_radius, cone_length)
        cone_mesh = Mesh(cone, material)

        cylinder = CylinderGeometry(radius=radius, length=cylinder_length)
        cylinder_mesh = Mesh(cylinder, material)

        cone_mesh.position.x = cylinder_length
        cone_mesh.rot.y = 90.
        cylinder_mesh.position.x = cylinder_length/2.
        cylinder_mesh.rot.y = 90.
        self.add(cone_mesh)
        self.add(cylinder_mesh)
        pass

class AxisObject(Object3D):
    #Red is X, Green is Y Blue is Z
    def __init__(self, x_mat = None, y_mat = None, z_mat = None, **kw):
        length=kw.pop('length', 0.1)
        radius=kw.pop('radius', length/14.)
        cone_radius = kw.pop('cone_radius', radius*2.)
        cone_length = kw.pop('cone_length', radius*3.)
        alpha=kw.pop('alpha', 1)
        super(AxisObject, self).__init__(**kw)
        if x_mat is None:
            red_mat = Material(color=(1,0,0), diffuse=(1,0,0), specular=(0.3,0.3,0.3), transparency=alpha, id_color=None)
        else:
            red_mat = x_mat
        if y_mat is None:

            green_mat = Material(color=(0,1,0), diffuse=(0,1,0), specular=(0.3,0.3,0.3), transparency=alpha, id_color=None)
        else:
            green_mat = y_mat

        if z_mat is None:

            blue_mat = Material(color=(0,0,1), diffuse=(0,0,1), specular=(0.3,0.3,0.3), transparency=alpha, id_color=None)
        else:
            blue_mat = z_mat
        x_axis = ArrowObject(red_mat, length=length, radius=radius, cone_radius=cone_radius, cone_length=cone_length)
        y_axis = ArrowObject(green_mat, length=length, radius=radius, cone_radius=cone_radius, cone_length=cone_length)
        z_axis = ArrowObject(blue_mat, length=length, radius=radius, cone_radius=cone_radius, cone_length=cone_length)

        z_axis.rot.y = -90
        y_axis.rot.z = +90

        self.add(x_axis)
        self.add(y_axis)
        self.add(z_axis)


class CircularProgressBar(Object3D):
    def __init__(self, sectors: int,
                 maximum_angle: float,
                 outer_radius: float,
                 inner_radius: float,
                 thickness: float,
                 start_color=(0., 1., 0.),
                 end_color=(1., 0., 0.),
                 **kw):
        super(CircularProgressBar, self).__init__(**kw)

        # self.minimum_angle = minimum_angle  # degrees
        self.maximum_angle = maximum_angle  # degrees

        outer_radius = outer_radius
        inner_radius = inner_radius
        thickness = thickness
        self.start_color = start_color
        self.end_color = end_color

        # self.sector_amount = math.ceil(self.maximum_angle/self.minimum_angle)

        self.segments = []
        self.removed_segments = []

        self.sectors = sectors

        self.material = Material(color=start_color, diffuse=start_color, specular=(0.3, 0.3, 0.3), transparency=1)

        base_angle = self.maximum_angle/(2**(self.sectors-1))
        for i in range(self.sectors):
            angle = base_angle * (2**(i-1))
            # if i == 0 or i == self.sectors - 1:
            if i == 0:
                angle = base_angle
            #     geometry = AnnularSector(outer_radius, inner_radius, thickness, angle, segments=(2**i) * 3)
            geometry = AnnularSector(outer_radius, inner_radius, thickness, angle, segments=(2**i) * 3, ends=False)
            object_3d = Mesh(geometry, self.material)
            object_3d.rot.y = 180
            object_3d.rot.z = angle
            self.add(object_3d)
            self.segments.append(object_3d)

        geometry = AnnularSector(outer_radius, inner_radius, thickness, base_angle, segments=3, ends=True)
        object_3d = Mesh(geometry, self.material)
        object_3d.rot.y = 180
        object_3d.rot.z = 0.
        self.add(object_3d)
        self.start_sector = object_3d

        geometry = AnnularSector(outer_radius+(outer_radius - inner_radius)*0.1,
                                 inner_radius-(outer_radius - inner_radius)*0.1,
                                 thickness*1.1,
                                 base_angle/3,
                                 segments=3, ends=True)
        material = Material(color=(0,0,0), diffuse=(0,0,0), specular=(0.3, 0.3, 0.3), transparency=1)
        object_3d = Mesh(geometry, material)
        object_3d.rot.y = 180
        object_3d.rot.z = -base_angle/6
        self.add(object_3d)
        self.start_marker = object_3d

        geometry = AnnularSector(outer_radius, inner_radius, thickness, base_angle, segments=3, ends=True)
        object_3d = Mesh(geometry, self.material)
        object_3d.rot.y = 180
        object_3d.rot.z = maximum_angle-base_angle
        self.add(object_3d)
        self.end_sector = object_3d


        geometry = TubeGeometry(outer_radius-(outer_radius - inner_radius)*0.1,
                                inner_radius + (outer_radius - inner_radius)*0.1,
                                thickness*0.2, circle_segment=64)
        material = Material(color=(1., 1., 1.), diffuse=(1., 1., 1.), specular=(0.3, 0.3, 0.3), transparency=0.3)
        white_progress = Mesh(geometry, material)
        self.add(white_progress)

        self.added_segments = [x for x in self.segments]

    def add_segment(self, i):
        if self.segments[i] not in self.added_segments:
            self.add_object(self.segments[i])
            self.added_segments.append(self.segments[i])
        pass

    def remove_segment(self, i):
        self.remove_object(self.segments[i])
        #self.children.remove(self.segments[i])
        # return
        if self.segments[i] in self.added_segments:
            self.added_segments.remove(self.segments[i])
        # self.removed_segments.append(self.segments[i])

    def set_progress(self, progress):

        if progress > 100:
            progress = 100.



        if progress < -100.:
            progress = -100
        if progress < 0:
            progress = -progress
            self.end_sector.set_rotation((180, self.end_sector.rot.y, self.end_sector.rot.z))
            self.start_sector.set_rotation((180, self.start_sector.rot.y, self.start_sector.rot.z))
            for i in self.segments:
                i.set_rotation((180, i.rot.y, i.rot.z))

        elif progress >= 0:
            self.end_sector.set_rotation((0., self.end_sector.rot.y, self.end_sector.rot.z))
            self.start_sector.set_rotation((0., self.start_sector.rot.y, self.start_sector.rot.z))
            for i in self.segments:
                i.set_rotation((0., i.rot.y, i.rot.z))

        # if progress == 0:

        color_hsv = [0.] * 3
        start_hsv = colorsys.rgb_to_hsv(*self.start_color)
        end_hsv = colorsys.rgb_to_hsv(*self.end_color)
        for i in range(len(start_hsv)):
            color_gradient = (end_hsv[i] - start_hsv[i]) / 100
            color_hsv[i] = color_gradient * float(progress) + start_hsv[i]

        color = colorsys.hsv_to_rgb(*color_hsv)
        color = tuple(color)
        self.material.__setattr__('color', color)
        self.material.__setattr__('diffuse', color)

        progress_angle = self.maximum_angle * (progress/100.)
        base_angle = self.maximum_angle/(2**(self.sectors-1))
        for i, segment in enumerate(self.segments):

            start_pos = 2**(i-1) * base_angle
            if i == 0:
                start_pos = 0
            end_pos = 2 ** (i) * base_angle

            if progress_angle > end_pos:
                self.add_segment(i)

                segment.rot.z = start_pos
                continue
            elif progress_angle < start_pos:
                self.remove_segment(i)
                continue
            else:
                self.add_segment(i)
                if i == 0:
                    segment.rot.z = 0
                else:
                    segment.rot.z = progress_angle - (2 ** (i - 1) * base_angle)

        self.end_sector.rot.z = max(progress_angle - base_angle, 0)


class VideoPlane(Object3D):
    def __init__(self, source, width, height, play=True, **kw):
        """ Creates a plane that plays a video (when loaded) along the XY plane with a width and height."""
        self.width = width
        self.height = height
        super(VideoPlane, self).__init__(**kw)
        self.video = Video(source=source, play=play)
        self.video.bind(loaded=self.on_video_load)


    def on_video_load(self, parent, value):
        # print("VIDEO LOADED")
        Clock.schedule_once(self.after_video_load, 0)

        pass

    def after_video_load(self, dt):
        texture = self.video.texture
        geometry = PlaneGeometry(self.width, self.height, texture=True)

        self.anim_material = Material(color=(0, 0, 0), diffuse=(0, 0, 0), specular=(0, 0, 0), map=texture,
                                      texture_ratio=1.0)
        self.plane_object = Mesh(geometry, self.anim_material)
        self.plane_object.pos.x = 0
        self.add_object(self.plane_object)
