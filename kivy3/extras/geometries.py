"""
The MIT License (MIT)

Copyright (c) 2013 Niko Skrypnik

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""
from typing import List, Tuple

from kivy3 import Vector3
from kivy3.core.geometry import Geometry
from kivy3.core.face3 import Face3
from kivy3.core.line2 import Line2

import math
import numpy as np


def normalise_v3(vector):
    length = math.sqrt(vector[
                           0] ** 2 + vector[1] ** 2 + vector[2] ** 2)
    new_vector = [x / length for x in vector]
    return new_vector


class BoxGeometry(Geometry):
    _cube_vertices = [(-1, 1, -1), (1, 1, -1),
                      (1, -1, -1), (-1, -1, -1),
                      (-1, 1, 1), (1, 1, 1),
                      (1, -1, 1), (-1, -1, 1),
                      ]

    _cube_lines = [(0, 1), (1, 2), (2, 3), (3, 0),
                   (4, 5), (5, 6), (6, 7), (7, 4),
                   (0, 4), (1, 5), (2, 6), (3, 7)]

    _cube_faces = [(0, 1, 2), (0, 2, 3), (2, 3, 6),
                   (3, 6, 7), (7, 6, 5), (7, 5, 4),
                   (4, 5, 1), (4, 1, 0), (4, 0, 3),
                   (7, 4, 3), (5, 1, 2), (6, 5, 2)
                   ]

    _cube_normals = [(0, 0, -1), (0, -1, 0), (0, 0, 1),
                     (0, 1, 0), (-1, 0, 0), (1, 0, 0)
                     ]

    def __init__(self, width, height, depth, **kw):
        """ X, Y, Z"""
        name = kw.pop('name', '')
        super(BoxGeometry, self).__init__(name)
        self.width_segment = kw.pop('width_segment', 1)
        self.height_segment = kw.pop('height_segment', 1)
        self.depth_segment = kw.pop('depth_segment', 1)

        self.w = width
        self.h = height
        self.d = depth

        self._build_box()

    def _build_box(self):

        for v in self._cube_vertices:
            v = Vector3(0.5 * v[0] * self.w,
                        0.5 * v[1] * self.h,
                        0.5 * v[2] * self.d)
            self.vertices.append(v)

        n_idx = 0
        for f in self._cube_faces:
            face3 = Face3(*f)
            normal = self._cube_normals[int(n_idx / 2)]
            face3.vertex_normals = [normal, normal, normal]
            n_idx += 1
            self.faces.append(face3)

        for l in self._cube_lines:
            self.lines.append(Line2(a=l[0], b=l[1]))


class CylinderGeometry(Geometry):
    def __init__(self, radius=0.5, length=1., **kw):
        name = kw.pop('name', '')
        super(CylinderGeometry, self).__init__(name)
        self.circle_segment = kw.pop('circle_segment', 16)
        self.depth_segment = kw.pop('depth_segment', 1)

        self.rad = radius
        self.length = length

        self._build_cylinder()

    def _build_cylinder(self):

        _cylinder_vertices = []
        top_vertices = []
        bottom_vertices = []
        cylinder_side_normals = []
        _cylinder_normals = []
        _cylinder_lines = []
        _cylinder_length_lines = []
        _cylinder_top_lines = []
        _cylinder_bottom_lines = []

        # Get vertices
        for i in range(self.circle_segment):
            x = math.cos(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.rad)
            y = math.sin(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.rad)

            n = Vector3(x, y, 0)
            n.normalize()
            cylinder_side_normals.append(n)

            top_vertices.append([x, y, 0.5 * float(self.length)])
            bottom_vertices.append([x, y, -0.5 * float(self.length)])

        _cylinder_vertices = top_vertices + bottom_vertices
        _cylinder_normals = cylinder_side_normals + cylinder_side_normals

        # Get lines
        for i in range(self.circle_segment):
            _cylinder_length_lines.append(Line2(i, i + self.circle_segment))
            if i == 0:
                _cylinder_top_lines.append(Line2(0, self.circle_segment - 1))
                _cylinder_bottom_lines.append(Line2(self.circle_segment, 2 * self.circle_segment - 1))
            else:
                _cylinder_top_lines.append(Line2(i - 1, i))
                _cylinder_bottom_lines.append(Line2(i - 1 + self.circle_segment, i + self.circle_segment))

        _cylinder_lines = _cylinder_length_lines + _cylinder_top_lines + _cylinder_bottom_lines

        for f in range(1, self.circle_segment - 1):
            # Top circle
            normal = Vector3(0, 0, 1)
            face = (0, f, f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        for f in range(self.circle_segment + 1, (2 * self.circle_segment) - 1):
            # Bottom circle
            normal = Vector3(0, 0, -1)
            face = (self.circle_segment, f, f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        for i in range(0, self.circle_segment):
            if i == (self.circle_segment - 1):
                face = (i, 0, i + self.circle_segment)
            else:
                face = (i, i + 1, i + self.circle_segment)
            face3 = Face3(*face)
            face3.vertex_normals = [_cylinder_normals[i], _cylinder_normals[i + 1],
                                    _cylinder_normals[i + self.circle_segment]]
            self.faces.append(face3)

            if i == (self.circle_segment - 1):
                face = (0, i + self.circle_segment, self.circle_segment)
                face3 = Face3(*face)
                face3.vertex_normals = (_cylinder_normals[i + 1], _cylinder_normals[i + self.circle_segment],
                                        _cylinder_normals[self.circle_segment])
            else:
                face = (i + 1, i + self.circle_segment, i + self.circle_segment + 1)

                face3 = Face3(*face)
                face3.vertex_normals = (_cylinder_normals[i + 1], _cylinder_normals[i + self.circle_segment],
                                        _cylinder_normals[i + self.circle_segment + 1])
            self.faces.append(face3)

        self.vertices = _cylinder_vertices
        self.lines = _cylinder_lines


class SphereGeometry(Geometry):

    def __init__(self, radius=1, sectors=36, stacks=18, **kw):
        name = kw.pop('name', '')
        super(SphereGeometry, self).__init__(name)
        self.stacks = stacks
        self.sectors = sectors
        self.rad = radius

        self._build_sphere()

    def _build_sphere(self):
        # Create Vertices and normals
        _vertices = []
        _normals = []
        # Top vertex
        _vertices.append((0, 0, 1. * self.rad))
        _normals.append((0, 0, 1))

        # Generate the faces
        for i in range(1, self.stacks):
            theta_1 = float(i) * (math.pi / self.stacks)
            z = math.cos(theta_1)
            for j in range(self.sectors):
                theta_2 = float(j) * (2. * math.pi / self.sectors)
                x = math.sin(theta_1) * math.cos(theta_2)
                y = math.sin(theta_1) * math.sin(theta_2)

                vertex = (x * self.rad, y * self.rad, z * self.rad)
                _vertices.append(vertex)
                _normals.append((x, y, z))

        # Bottom vertex
        _vertices.append((0, 0, -1. * self.rad))
        _normals.append((0, 0, -1.))

        # Generate the Faces with mapping to the vertices

        # Top one
        for i in range(1, self.sectors + 1):

            a = 0
            b = i
            c = i + 1
            if c == self.sectors + 1:
                c = 1
            face3 = Face3(a, b, c)
            face3.vertex_normals = [_normals[a], _normals[b], _normals[c]]
            self.faces.append(face3)

        # Stacks:
        for i in range(0, self.stacks - 2):
            top_idx = (i * self.sectors + 1, (i + 1) * self.sectors + 1)
            for j in range(top_idx[0], top_idx[1]):
                a = j
                b = j + 1
                c = j + self.sectors
                if b == top_idx[1]:
                    b = top_idx[0]
                face3 = Face3(a, b, c)
                face3.vertex_normals = [_normals[a], _normals[b], _normals[c]]
                self.faces.append(face3)

                a = j + 1
                b = j + self.sectors
                c = j + self.sectors + 1
                if a == top_idx[1]:
                    a = top_idx[0]
                if c == top_idx[1] + self.sectors:
                    c = top_idx[1]

                face3 = Face3(a, b, c)
                face3.vertex_normals = [_normals[a], _normals[b], _normals[c]]
                self.faces.append(face3)

        for i in range(len(_vertices) - 1 - self.sectors, len(_vertices) - 1):

            a = len(_vertices) - 1
            b = i
            c = i + 1
            if c == len(_vertices) - 1:
                c = len(_vertices) - 1 - self.sectors
            face3 = Face3(a, b, c)
            face3.vertex_normals = [_normals[a], _normals[b], _normals[c]]
            self.faces.append(face3)

        self.vertices = _vertices


class PlaneGeometry(Geometry):
    def __init__(self, width, length, texture=None, seg_width=2, seg_length=2, axis="xy", **kw):
        name = kw.pop('name', '')
        super(PlaneGeometry, self).__init__(name)
        self.texture = texture
        self.w = float(width)
        self.l = float(length)
        self.seg_width = seg_width
        self.seg_length = seg_length
        self.axis = axis
        self._build_plane()

    def _build_plane(self):

        _vertices = []
        _texture_uvs = []
        _faces = []

        _vertices = [(-0.5, -0.5, 0.),
                     (0.5, -0.5, 0.),
                     (-0.5, 0.5, 0.),
                     (0.5, 0.5, 0.)]

        _faces = [(0, 1, 2), (1, 2, 3)]

        _texture_uvs = [(0., 1.), (1., 1.), (0., 0.), (1., 0.)]

        for v in _vertices:
            if self.axis == "xy":
                v = Vector3(0.5 * v[0] * self.w,
                            0.5 * v[1] * self.l,
                            0)
            elif self.axis == "xz":
                v = Vector3(0.5 * v[0] * self.w,
                            0,
                            0.5 * v[1] * self.l)
            elif self.axis == "yz":
                v = Vector3(0,
                            0.5 * v[0] * self.w,
                            0.5 * v[1] * self.l)
            self.vertices.append(v)

        for f in _faces:
            face3 = Face3(*f)
            normal = (0., 0., 1.)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)
            if self.texture is not None:
                for i in f:
                    self.face_vertex_uvs[0].append(_texture_uvs[i])


class ConeGeometry(Geometry):
    def __init__(self, radius, length, **kw):
        name = kw.pop('name', '')
        super(ConeGeometry, self).__init__(name)
        self.circle_segment = kw.pop('circle_segment', 16)

        self.rad = radius
        self.length = length

        self._build_cone()

    def _build_cone(self):
        _vertices = []
        _vertex_normals = []
        top_vertex = (0, 0, self.length)
        _vertices.append(top_vertex)
        _vertex_normals.append((0, 0, 1))

        for i in range(self.circle_segment):
            x = math.cos(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.rad)
            y = math.sin(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.rad)
            _vertex_normals.append((x / self.rad, y / self.rad, 0))
            _vertices.append((x, y, 0))

        for f in range(2, self.circle_segment):
            # Bottom circle
            normal = Vector3(0, 0, -1)
            face = (1, f, f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)
        self.vertices = _vertices
        for f in range(1, self.circle_segment + 1):
            # trialngle corners
            # normal = Vector3(0,0,-1)
            if (f + 1 == self.circle_segment + 1):
                face = (0, f, 1)
                face3 = Face3(*face)
                normal = self.calculate_normal(face3)
                face3.vertex_normals = [normal, _vertex_normals[f], _vertex_normals[1]]
            else:
                face = (0, f, f + 1)
                face3 = Face3(*face)
                normal = self.calculate_normal(face3)
                face3.vertex_normals = [normal, _vertex_normals[f], _vertex_normals[f + 1]]

            self.faces.append(face3)


class GridGeometry(Geometry):
    """
        Generate a grid on xy plane centered about origin.
        To create grid on another plane, use this geometry
        in a Lines() object and rotate/translate the mesh.

    """

    def __init__(self, size=(10, 10), spacing=1, **kw):
        """
        :param size: Size of grid. Tuple (width, length)
        :param spacing: Distance between lines
        """
        name = kw.pop('name', '')
        super(GridGeometry, self).__init__(name)

        self.size = size
        self.spacing = spacing
        self._build_grid()

    def _build_grid(self):
        axes = 2
        v_idx = 0
        for axis in range(axes):
            # number of lines to draw
            num_lines = int(self.size[axis] / self.spacing) + 1

            for line_idx in range(num_lines):
                # work out starting and ending positions
                start_x = -(self.size[axis] / 2) + (line_idx * self.spacing)
                start_y = -(self.size[1 - axis] / 2)
                end_x = start_x
                end_y = start_y + self.size[1 - axis]

                if axis == 0:
                    v_front = Vector3(start_x, start_y, 0)
                    v_back = Vector3(end_x, end_y, 0)
                elif axis == 1:
                    v_front = Vector3(start_y, start_x, 0)
                    v_back = Vector3(end_y, end_x, 0)

                self.vertices.append(v_front)
                self.vertices.append(v_back)

                self.lines.append(Line2(a=v_idx, b=v_idx + 1))
                v_idx += 2


class TubeGeometry(Geometry):

    def __init__(self, outer_radius, inner_radius, length, **kw):
        name = kw.pop('name', '')
        super(TubeGeometry, self).__init__(name)
        self.length = length
        self.outer_radius = outer_radius
        self.inner_radius = inner_radius

        self.circle_segment = kw.pop('circle_segment', 16)

        self._build_tube()

    def _build_tube(self):
        _tube_vertices = []
        top_outer_vertices = []
        bottom_outer_vertices = []
        top_inner_vertices = []
        bottom_inner_vertices = []
        cylinder_outer_side_normals = []
        cylinder_inner_side_normals = []

        _tube_normals = []
        # _cylinder_lines = []
        # _cylinder_length_lines = []
        # _cylinder_top_lines = []
        # _cylinder_bottom_lines = []

        cs = self.circle_segment

        for i in range(self.circle_segment):
            x_o = math.cos(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.outer_radius)
            y_o = math.sin(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.outer_radius)
            x_i = math.cos(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.inner_radius)
            y_i = math.sin(float(i) * (2. * math.pi) / float(self.circle_segment)) * float(self.inner_radius)
            n = Vector3(x_o, y_o, 0)
            n.normalize()
            cylinder_outer_side_normals.append(n)
            n = Vector3(-x_o, -y_o, 0)
            n.normalize()
            cylinder_inner_side_normals.append(n)
            top_outer_vertices.append([x_o, y_o, 0.5 * float(self.length)])
            bottom_outer_vertices.append([x_o, y_o, -0.5 * float(self.length)])

            top_inner_vertices.append([x_i, y_i, 0.5 * float(self.length)])
            bottom_inner_vertices.append([x_i, y_i, -0.5 * float(self.length)])

        _tube_vertices = top_outer_vertices + top_inner_vertices + bottom_outer_vertices + bottom_inner_vertices
        # _tube_normals = cylinder_outer_side_normals + cylinder_outer_side_normals

        normal = Vector3(0, 0, 1)
        for f in range(0, cs - 1):
            # Top circle
            face = (f, f + 1, cs + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

            face = (f, cs + f, cs + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        # last edge
        face = (cs - 1, 0, cs)
        face3 = Face3(*face)
        face3.vertex_normals = [normal, normal, normal]
        self.faces.append(face3)
        face = (cs - 1, 2 * cs - 1, cs)
        face3 = Face3(*face)
        face3.vertex_normals = [normal, normal, normal]
        self.faces.append(face3)
        self.vertices = _tube_vertices

        normal = Vector3(0, 0, -1)
        for f in range(0, cs - 1):
            # Top circle
            face = (f + 2 * cs, f + 1 + 2 * cs, cs + f + 1 + 2 * cs)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

            face = (f + 2 * cs, cs + f + 2 * cs, cs + f + 1 + 2 * cs)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        # last edge
        face = (cs - 1 + 2 * cs, 0 + 2 * cs, cs + 2 * cs)
        face3 = Face3(*face)
        face3.vertex_normals = [normal, normal, normal]
        self.faces.append(face3)
        face = (cs - 1 + 2 * cs, 2 * cs - 1 + 2 * cs, cs + 2 * cs)
        face3 = Face3(*face)
        face3.vertex_normals = [normal, normal, normal]
        self.faces.append(face3)
        self.vertices = _tube_vertices

        # outer cylinder
        for f in range(cs - 1):
            face = (f, f + 1, 2 * cs + f)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_outer_side_normals[f], cylinder_outer_side_normals[f + 1],
                                    cylinder_outer_side_normals[f]]
            self.faces.append(face3)

            face = (f + 1, 2 * cs + f, 2 * cs + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_outer_side_normals[f + 1], cylinder_outer_side_normals[f],
                                    cylinder_outer_side_normals[f + 1]]
            self.faces.append(face3)

        face = (cs - 1, 0, 3 * cs - 1)
        face3 = Face3(*face)
        face3.vertex_normals = [cylinder_outer_side_normals[cs - 1], cylinder_outer_side_normals[0],
                                cylinder_outer_side_normals[cs - 1]]
        self.faces.append(face3)

        face = (0, 3 * cs - 1, 2 * cs)
        face3 = Face3(*face)
        face3.vertex_normals = [cylinder_outer_side_normals[0], cylinder_outer_side_normals[cs - 1],
                                cylinder_outer_side_normals[0]]
        self.faces.append(face3)

        # inner cylinder
        for f in range(cs - 1):
            face = (f + cs, f + 1 + cs, 3 * cs + f)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_inner_side_normals[f], cylinder_inner_side_normals[f + 1],
                                    cylinder_inner_side_normals[f]]
            self.faces.append(face3)

            face = (f + 1 + cs, 3 * cs + f, 3 * cs + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_inner_side_normals[f + 1], cylinder_inner_side_normals[f],
                                    cylinder_inner_side_normals[f + 1]]
            self.faces.append(face3)

        face = (2 * cs - 1, cs, 4 * cs - 1)
        face3 = Face3(*face)
        face3.vertex_normals = [cylinder_inner_side_normals[cs - 1], cylinder_inner_side_normals[0],
                                cylinder_inner_side_normals[cs - 1]]
        self.faces.append(face3)

        face = (cs, 4 * cs - 1, 3 * cs)
        face3 = Face3(*face)
        face3.vertex_normals = [cylinder_inner_side_normals[0], cylinder_inner_side_normals[cs - 1],
                                cylinder_inner_side_normals[0]]
        self.faces.append(face3)

    pass


class AnnularSector(Geometry):
    def __init__(self, outer_radius: float, inner_radius: float, length: float, angle: float, segments=16, ends=True, **kw):
        """ Builds and annular sector anti clockwise from the x axis

        :param outer_radius:
        :param inner_radius:
        :param length:
        :param angle:  # Degrees
        :param segments:
        :param kw:
        """
        name = kw.pop("name", "")
        super(AnnularSector, self).__init__(name)
        self.length: float = length
        self.outer_radius: float = outer_radius
        self.inner_radius: float = inner_radius
        self.angle: float = angle
        self.segments = segments
        self._build_annular_sector(ends)

    def _build_annular_sector(self, ends):

        annular_sector_vertices = []
        top_inner_vertices = []
        top_outer_vertices = []
        bottom_inner_vertices = []
        bottom_outer_vertices = []
        cylinder_outer_side_normals = []
        cylinder_inner_side_normals = []

        cs = self.segments
        # Bottom inner vertices
        for i in range(int(cs+1)):
            x_i = math.cos(float(i) * math.radians(self.angle) / float(cs)) * self.inner_radius
            x_o = math.cos(float(i) * math.radians(self.angle) / float(cs)) * self.outer_radius
            y_i = math.sin(float(i) * math.radians(self.angle) / float(cs)) * self.inner_radius
            y_o = math.sin(float(i) * math.radians(self.angle) / float(cs)) * self.outer_radius
            n = Vector3(x_o, y_o, 0)
            n.normalize()
            cylinder_outer_side_normals.append(n)
            n = Vector3(-x_o, -y_o, 0)
            n.normalize()
            cylinder_inner_side_normals.append(n)
            top_outer_vertices.append([x_o, y_o, 0.5 * float(self.length)])
            bottom_outer_vertices.append([x_o, y_o, -0.5 * float(self.length)])
            top_inner_vertices.append([x_i, y_i, 0.5 * float(self.length)])
            bottom_inner_vertices.append([x_i, y_i, -0.5 * float(self.length)])

        annular_sector_vertices = top_outer_vertices + top_inner_vertices + \
                                  bottom_outer_vertices + bottom_inner_vertices

        #Top Face
        normal = Vector3(0, 0, 1)
        for f in range(0, int(cs)):
            # Top circle
            face = (f, f + 1, cs+1 + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

            face = (f, cs+1 + f, cs+1 + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        normal = Vector3(0, 0, -1)
        for f in range(0, cs):
            # Top circle
            face = (f + 2 * (cs+1), f + 1 + 2 * (cs+1), (cs+1) + f + 1 + 2 * (cs+1))
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

            face = (f + 2 * (cs+1), (cs+1) + f + 2 * (cs+1), (cs+1) + f + 1 + 2 * (cs+1))
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        for f in range(cs):
            face = (f, f + 1, 2 * (cs+1) + f)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_outer_side_normals[f], cylinder_outer_side_normals[f + 1],
                                    cylinder_outer_side_normals[f]]
            self.faces.append(face3)

            face = (f + 1, 2 * (cs+1) + f, 2 * (cs+1) + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_outer_side_normals[f + 1], cylinder_outer_side_normals[f],
                                    cylinder_outer_side_normals[f + 1]]
            self.faces.append(face3)

        # inner cylinder
        for f in range(cs):
            face = (f + (cs+1), f + 1 + (cs+1), 3 * (cs+1) + f)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_inner_side_normals[f], cylinder_inner_side_normals[f + 1],
                                    cylinder_inner_side_normals[f]]
            self.faces.append(face3)

            face = (f + 1 + (cs+1), 3 * (cs+1) + f, 3 * (cs+1) + f + 1)
            face3 = Face3(*face)
            face3.vertex_normals = [cylinder_inner_side_normals[f + 1], cylinder_inner_side_normals[f],
                                    cylinder_inner_side_normals[f + 1]]
            self.faces.append(face3)

        if ends:
            # Front End:
            normal = Vector3(0, 1, 0)
            face = (0, cs+1, 2*(cs+1))
            face3 = Face3(*face)

            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

            face = (cs + 1, 2 * (cs + 1), 3*(cs+1))
            face3 = Face3(*face)
            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

            # Back End:
            normal = Vector3(math.sin(math.radians(self.angle) + math.pi/4.),
                             math.cos(math.radians(self.angle) + math.pi/4.), 0)

            face = ((cs+1)-1, cs+1 + (cs+1)-1, 2*(cs+1) + (cs+1)-1)
            face3 = Face3(*face)

            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)
            face = (cs + 1 + (cs + 1) - 1, 2 * (cs + 1) + (cs + 1) - 1,  3 * (cs + 1) + (cs + 1) - 1)
            face3 = Face3(*face)

            face3.vertex_normals = [normal, normal, normal]
            self.faces.append(face3)

        self.vertices = annular_sector_vertices


class CapsuleGeometry(Geometry):
    """ This class generates a geometry for a capsule.

    """

    def __init__(self, radius: float, length: float, **kw):
        """ Generate a Capsule with length and radius with length representing the distance between the centre of the
        two hemispheres on the end of the capsules. The capsules will have a length going along the Z axis, with the
        origin placed at the centre of the capsule.

        :param radius: Radius in m
        :param length: Length in m
        :param kw:
        """
        name: str = kw.pop('name', '')
        super(CapsuleGeometry, self).__init__(name)

        self.circle_segments: int = kw.pop('circle_segments', 32)
        self.radius: float = radius
        self.length: float = length

        self._build_capsule()

    def _build_capsule(self):
        segments: int = self.circle_segments
        length: float = self.length
        radius: float = self.radius
        # 1. Generate Vertices and normals for cylinder
        cylinder_vertices: List[Tuple[float, float, float]]
        cylinder_normals: List[Vector3]

        top_vertices: List[Tuple[float, float, float]] = []
        bottom_vertices: List[Tuple[float, float, float]] = []
        cylinder_side_normals: List[Vector3] = []
        for i in range(segments):
            x: float = math.cos(float(i) * (2. * math.pi) / float(segments)) * float(radius)
            y: float = math.sin(float(i) * (2. * math.pi) / float(segments)) * float(radius)
            n: Vector3 = Vector3(x, y, 0)
            n.normalize()
            cylinder_side_normals.append(n)

            top_vertices.append((x, y, 0.5 * length))
            bottom_vertices.append((x, y, -0.5 * length))

        cylinder_vertices: List[Tuple[float, float, float]] = top_vertices + bottom_vertices

        # Duplicate normals for bottom and top cylinder edge.
        cylinder_normals: List[Vector3] = cylinder_side_normals*2

        # 2. Generate Faces for cylinder edge.
        for i in range(segments):
            if i == (segments - 1):
                face: Tuple[int, int, int] = (i, 0, i + segments)
            else:
                face: Tuple[int, int, int] = (i, i + 1, i + segments)
            face3: Face3 = Face3(*face)
            face3.vertex_normals = [cylinder_normals[i], cylinder_normals[i+1], cylinder_normals[i + segments]]
            self.faces.append(face3)

            if i == (segments - 1):
                face: Tuple[int, int, int] = (0, i + segments, segments)
                face3: Face3 = Face3(*face)
                face3.vertex_normals = (cylinder_normals[i + 1],
                                        cylinder_normals[i + segments],
                                        cylinder_normals[segments])
            else:
                face: Tuple[int, int, int] = (i + 1, i + segments, i + segments + 1)
                face3: Face3 = Face3(*face)
                face3.vertex_normals = (cylinder_normals[i + 1],
                                        cylinder_normals[i + segments],
                                        cylinder_normals[i + segments + 1])
            self.faces.append(face3)

        # 3. Generate Vertices and Normals for hemispheres
        # Generate a sphere with the bottom half translated down and the top half translated up.
        sphere_vertices: List[Tuple[float, float, float]] = []
        sphere_normals: List[Vector3] = []
        radius: float = self.radius
        cylinder_length: float = self.length

        top_hemisphere_vertices: List[Tuple[float, float, float]] = []
        top_hemisphere_normals: List[Tuple[float, float, float]] = []
        bottom_hemisphere_vertices:  List[Tuple[float, float, float]] = []
        bottom_hemisphere_normals: List[Tuple[float, float, float]] = []
        stacks: int = math.ceil(self.circle_segments/2)  # The about of layers a sphere has
        sectors: int = int(self.circle_segments)

        top_hemisphere_vertices.append((0., 0., 1. * radius + cylinder_length/2))
        top_hemisphere_normals.append((0., 0., 1.))

        bottom_hemisphere_vertices.append((0., 0., - 1. * radius - + cylinder_length/2))
        bottom_hemisphere_normals.append((0., 0., -1.))
        for i in range(1, stacks+1):
            theta_1: float = i * (math.pi / 2) / stacks
            z: float = math.cos(theta_1)
            for j in range(sectors):
                theta_2: float = j * (2. * math.pi) / sectors
                x: float = math.sin(theta_1) * math.cos(theta_2)
                y: float = math.sin(theta_1) * math.sin(theta_2)

                top_hemisphere_vertices.append((x*radius, y*radius, z*radius + cylinder_length/2))
                top_hemisphere_normals.append((x, y, z))
                bottom_hemisphere_vertices.append((x*radius, y*radius, -z*radius - cylinder_length/2))
                bottom_hemisphere_normals.append((x, y, -z))

        # 4. Generate Faces for hemispheres.

        #  4.a Generate faces for top hemisphere
        index_offset = len(cylinder_vertices)  # Offset due to the existence of the vertices
        for i in range(1, sectors+1):
            a: int = 0
            b: int = i
            c: int = i + 1
            if c == sectors + 1:
                c = 1
            face3: Face3 = Face3(a + index_offset, b + index_offset, c + index_offset)
            face3.vertex_normals = [top_hemisphere_normals[a], top_hemisphere_normals[b], top_hemisphere_normals[c]]
            self.faces.append(face3)

        for i in range(0, stacks-1):
            top_idx: Tuple[int, int] = (i * sectors + 1, (i+1) * sectors + 1)
            for j in range(top_idx[0], top_idx[1]):
                a: int = j
                b: int = j + 1
                c: int = j + sectors

                if b == top_idx[1]:
                    b = top_idx[0]

                face3: Face3 = Face3(a + index_offset, b + index_offset, c + index_offset)
                face3.vertex_normals = [top_hemisphere_normals[a], top_hemisphere_normals[b], top_hemisphere_normals[c]]
                self.faces.append(face3)

                a: int = j + 1
                b: int = j + sectors
                c: int = j + sectors + 1
                if a == top_idx[1]:
                    a = top_idx[0]
                if c == top_idx[1] + sectors:
                    c = top_idx[1]

                face3: Face3 = Face3(a + index_offset, b + index_offset, c + index_offset)
                face3.vertex_normals = [top_hemisphere_normals[a], top_hemisphere_normals[b], top_hemisphere_normals[c]]
                self.faces.append(face3)

        #  4.ba Generate faces for bottom hemisphere
        index_offset = len(cylinder_vertices) + len(top_hemisphere_vertices) # Offset due to the existence of the vertices
        for i in range(1, sectors + 1):
            a: int = 0
            b: int = i
            c: int = i + 1
            if c == sectors + 1:
                c = 1
            face3: Face3 = Face3(a + index_offset, b + index_offset, c + index_offset)
            face3.vertex_normals = [bottom_hemisphere_normals[a], bottom_hemisphere_normals[b],
                                    bottom_hemisphere_normals[c]]
            self.faces.append(face3)

        for i in range(0, stacks - 1):
            top_idx: Tuple[int, int] = (i * sectors + 1, (i + 1) * sectors + 1)
            for j in range(top_idx[0], top_idx[1]):
                a: int = j
                b: int = j + 1
                c: int = j + sectors

                if b == top_idx[1]:
                    b = top_idx[0]

                face3: Face3 = Face3(a + index_offset, b + index_offset, c + index_offset)
                face3.vertex_normals = [bottom_hemisphere_normals[a], bottom_hemisphere_normals[b],
                                        bottom_hemisphere_normals[c]]
                self.faces.append(face3)

                a: int = j + 1
                b: int = j + sectors
                c: int = j + sectors + 1
                if a == top_idx[1]:
                    a = top_idx[0]
                if c == top_idx[1] + sectors:
                    c = top_idx[1]

                face3: Face3 = Face3(a + index_offset, b + index_offset, c + index_offset)
                face3.vertex_normals = [bottom_hemisphere_normals[a], bottom_hemisphere_normals[b],
                                        bottom_hemisphere_normals[c]]
                self.faces.append(face3)
        # 5. Add to self.faces and self.vertices

        self.vertices = cylinder_vertices + top_hemisphere_vertices + bottom_hemisphere_vertices


        pass
